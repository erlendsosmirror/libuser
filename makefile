###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = libuser
PROJECT_FILES = $(shell find src -name "*.*")

###############################################################################
# Compiler flags, file processing and standard targets
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets

###############################################################################
# Makefile execution
###############################################################################
all: build/$(PROJECT_NAME).a x86lib

build/$(PROJECT_NAME).a: $(OBJECTS)
	@arm-none-eabi-ar rcs $@ $(OBJECTS)

x86lib:
	@mkdir -p build/x86
	@gcc -c src/simpleprint/simpleprint.c -o build/x86/simpleprint.o
	@ar rcs build/$(PROJECT_NAME)x86.a build/x86/simpleprint.o
