/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include "display/lcd.h"

///////////////////////////////////////////////////////////////////////////////
// Private vars
///////////////////////////////////////////////////////////////////////////////
int lcd_dev = -1;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

// LCD Specific functions

int LCDInit (void)
{
	lcd_dev = open ("/dev/lcd", O_RDWR);
	return lcd_dev;
}

void LCDDeInit (void)
{
	if (lcd_dev >= 0)
		close (lcd_dev);
}

void LCDRotate (int direction)
{
	ioctl (lcd_dev, 9, direction);
}

void LCDSetAddress (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
{
	unsigned int c[] = {x1, y1, x2, y2};
	ioctl (lcd_dev, 10, (int)c);
}

// Helper function, does not interface LCD

unsigned int LCDColor (unsigned int r, unsigned int g, unsigned int b)
{
	// rrrrrggggggbbbbb
	unsigned int temp = 0;
	temp = ((r & 0xF8) << 8) | ((g & 0xFC) << 3) | ((b & 0xF8) >> 3);
	return temp;
}

// Generic draw functions

void LCDText (char *str, unsigned int x, unsigned int y, int fontsize, unsigned int fcolor, unsigned int bcolor)
{
	unsigned int c[] = {(unsigned int)str, x, y, fontsize, fcolor, bcolor};
	ioctl (lcd_dev, 0, (int)c);
}

void LCDChar (char c, unsigned int x, unsigned int y, int fontsize, unsigned int fcolor, unsigned int bcolor)

{
	unsigned int cc[] = {(unsigned int)c, x, y, fontsize, fcolor, bcolor};
	ioctl (lcd_dev, 1, (int)cc);
}
void LCDRectangle (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color)
{
	unsigned int c[] = {x1, y1, x2, y2, color};
	ioctl (lcd_dev, 2, (int)c);
}

void LCDVLine (unsigned int y1, unsigned int y2, unsigned int x, unsigned int color)
{
	unsigned int c[] = {y1, y2, x, color};
	ioctl (lcd_dev, 3, (int)c);
}

void LCDHLine (unsigned int x1, unsigned int x2, unsigned int y, unsigned int color)
{
	unsigned int c[] = {x1, x2, y, color};
	ioctl (lcd_dev, 4, (int)c);
}

void LCDLine (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color)
{
	unsigned int c[] = {x1, y1, x2, y2, color};
	ioctl (lcd_dev, 5, (int)c);
}

void LCDDot (unsigned int x, unsigned int y, unsigned int color)
{
	unsigned int c[] = {x, y, color};
	ioctl (lcd_dev, 6, (int)c);
}

void LCDBox (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int color)
{
	unsigned int c[] = {x1, y1, x2, y2, color};
	ioctl (lcd_dev, 7, (int)c);
}

void LCDFill (unsigned int color)
{
	unsigned int c = color;
	ioctl (lcd_dev, 8, (int)c);
}

void LCDData (unsigned short *data, int n)
{
	unsigned int c[] = {(unsigned int)data, n};
	ioctl (lcd_dev, 11, (int)c);
}

void LCDGetMetrics (unsigned int *w, unsigned int *h)
{
	unsigned int c[] = {(unsigned int)w, (unsigned int)h};
	ioctl (lcd_dev, 12, (int)c);
}
