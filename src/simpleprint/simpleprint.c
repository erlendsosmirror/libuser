/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void SimplePrint (char *str)
{
	int len = strlen(str);
	int n = len;
	int pos = 0;

	do
	{
		int diff = write (1, str + pos, n);

		if (diff < 0)
			return;

		pos += diff;
		n -= diff;
	} while (pos != len);
}

int PrintError (char *programname, char *filename)
{
	int err = errno;
	SimplePrint (programname);
	SimplePrint (": ");
	SimplePrint (filename);
	SimplePrint (": ");
	SimplePrint (strerror (err));
	SimplePrint ("\n");
	return errno;
}

void Reverse (char *s)
{
	for (int i = 0, j = strlen(s)-1; i < j; i++, j--)
	{
		char c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void _itoa (int n, char *str)
{
	int sign = n;

	if (n < 0)
		n = -n;

	int i = 0;

	do {
		str[i++] = '0' + n % 10;
	} while ((n /= 10));

	if (sign < 0)
		str[i++] = '-';

	str[i] = 0;
	Reverse(str);
}

void _itoa_withzeroes (int n, char *str)
{
	int sign = n;

	if (n < 0)
		n = -n;

	int i = 0;

	// The compiler replaces it with memset when written as a for loop
	do {
		str[i++] = '0' + n % 10;
	} while ((n /= 10));

	while (i < 10)
		str[i++] = '0';

	str[i++] = (sign < 0) ? '-' : '+';

	str[i] = 0;
	Reverse(str);
}

void PrintValue (char *name, int val)
{
	char str[16];
	_itoa (val, str);
	SimplePrint (name);
	SimplePrint (": ");
	SimplePrint (str);
	SimplePrint ("\n");
}

void itoa_uint32_hex (unsigned int in, char *out)
{
	// http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa
	unsigned char b;

	b = (in >> 28) & 0x0F;
	out[0] = (55 + b + (((b-10)>>31)&-7));
	b = (in >> 24) & 0x0F;
	out[1] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 20) & 0x0F;
	out[2] = (55 + b + (((b-10)>>31)&-7));
	b = (in >> 16) & 0x0F;
	out[3] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 12) & 0x0F;
	out[4] = (55 + b + (((b-10)>>31)&-7));
	b = (in >> 8) & 0x0F;
	out[5] = (55 + b + (((b-10)>>31)&-7));

	b = (in >> 4) & 0x0F;
	out[6] = (55 + b + (((b-10)>>31)&-7));
	b = (in >> 0) & 0x0F;
	out[7] = (55 + b + (((b-10)>>31)&-7));
	out[8] = 0;
}

void PrintValueHex (char *name, int val)
{
	char str[16];
	itoa_uint32_hex (val, str);
	SimplePrint (name);
	SimplePrint (": ");
	SimplePrint (str);
	SimplePrint ("h\n");
}
